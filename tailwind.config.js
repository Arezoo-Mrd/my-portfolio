/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        secondary: {
          100: '#456B91',
          300: '#34495E',
          500: '#181818',
          700: '#181818'
        },
        white: {
          100: '#FFFFFF',
          300: '#DADFE1',
          500: '#BDC3C7'
        }
      }
    },
  },
  plugins: [],
}
