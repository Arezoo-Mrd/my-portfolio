importScripts("https://www.gstatic.com/firebasejs/7.9.1/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/7.9.1/firebase-messaging.js");

if (!firebase.apps.length) {
    firebase.initializeApp({
        apiKey: "AIzaSyCzjLrJfcTmfA33gFYr-zPy4eP3oVItLdQ",
        authDomain: "portfolio-780e4.firebaseapp.com",
        projectId: "portfolio-780e4",
        storageBucket: "portfolio-780e4.appspot.com",
        messagingSenderId: "718968519696",
        appId: "1:718968519696:web:61d644e4cba1b60fa05e95"
    });

    const messaging = firebase.messaging();
    messaging.setBackgroundMessageHandler((payload) => console.log('payload', payload));
}