import "firebase/messaging";
import firebase from "firebase/app";
import localforage from "localforage";

const firebaseCloudMessaging = {
    init: async () => {
        if (!firebase?.apps?.length) {

            // Initialize the Firebase app with the credentials
            firebase?.initializeApp({
                apiKey: "AIzaSyCzjLrJfcTmfA33gFYr-zPy4eP3oVItLdQ",
                authDomain: "portfolio-780e4.firebaseapp.com",
                projectId: "portfolio-780e4",
                storageBucket: "portfolio-780e4.appspot.com",
                messagingSenderId: "718968519696",
                appId: "1:718968519696:web:61d644e4cba1b60fa05e95"
            });

            try {
                const messaging = firebase.messaging();
                const tokenInLocalForage = await localforage.getItem("fcm_token");

                // Return the token if it is alredy in our local storage
                if (tokenInLocalForage !== null) {
                    return tokenInLocalForage;
                }

                // Request the push notification permission from browser
                const status = await Notification.requestPermission();
                if (status && status === "granted") {
                    // Get new token from Firebase
                    const fcm_token = await messaging.getToken({
                        vapidKey: "your_web_push_certificate_key_pair",
                    });

                    // Set token in our local storage
                    if (fcm_token) {
                        localforage.setItem("fcm_token", fcm_token);
                        return fcm_token;
                    }
                }
            } catch (error) {
                console.error(error);
                return null;
            }
        }
    },
};
export { firebaseCloudMessaging };

// const firebaseConfig = {
//     apiKey: "AIzaSyCzjLrJfcTmfA33gFYr-zPy4eP3oVItLdQ",
//     authDomain: "portfolio-780e4.firebaseapp.com",
//     projectId: "portfolio-780e4",
//     storageBucket: "portfolio-780e4.appspot.com",
//     messagingSenderId: "718968519696",
//     appId: "1:718968519696:web:61d644e4cba1b60fa05e95"
// };


// const firebaseApp = initializeApp(firebaseConfig);
// const messaging = getMessaging(firebaseApp);

// export const getToken = (setTokenFound) => {
//     return getToken(messaging, { vapidKey: 'GENERATED_MESSAGING_KEY' }).then((currentToken) => {
//         if (currentToken) {
//             console.log('current token for client: ', currentToken);
//             setTokenFound(true);
//             // Track the token -> client mapping, by sending to backend server
//             // show on the UI that permission is secured
//         } else {
//             console.log('No registration token available. Request permission to generate one.');
//             setTokenFound(false);
//             // shows on the UI that permission is required
//         }
//     }).catch((err) => {
//         console.log('An error occurred while retrieving token. ', err);
//         // catch error while creating client token
//     });
// }


// export default firebase
