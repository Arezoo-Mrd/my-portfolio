import React from "react";

const SkillProgress = ({ item, percent }) => {
  return (
    <div className="w-full py-5">
      <h6>{item}</h6>
      <div className="relative w-full h-2 bg-gray-400 ">
        <div
          className="absolute left-0 h-full bg-secondary-100"
          style={{ width: `${percent}%` }}
        >
          <div className="absolute right-0 font-bold bottom-full">
            {percent}%
          </div>
        </div>
      </div>
    </div>
  );
};

export default SkillProgress;
