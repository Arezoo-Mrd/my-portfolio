import React from "react";

const Tabs = () => {
  return (
    <div className="flex flex-wrap justify-center max-w-full">
      <input
        className="hidden tabs_radio_button"
        name="tabs-example"
        type="radio"
        id="tab1"
        checked
      />
      <label className="p-5 cursor-pointer " htmlFor="tab1">
        tab1
      </label>
      <div className="order-1 hidden w-full p-5 content bg-white-500">
        Content of tab 1
      </div>

      <input
        className="hidden tabs_radio_button"
        name="tabs-example"
        type="radio"
        id="tab2"
      />
      <label className="p-5 cursor-pointer" htmlFor="tab2">
        tab2
      </label>
      <div className="order-1 hidden w-full p-5 content bg-white-500">
        Content of tab 2
      </div>
    </div>
  );
};

export default Tabs;
