import ArrowRight from "./../../public/assets/svgs/arrow-right.svg";

const About = () => {
  const sampleData = [
    { title: "Birthday:", value: "14 May 1992" },
    { title: "Phone:", value: "+98 921 441 39 41" },
    { title: "City:", value: "Isfahan, Falavarjan" },
    { title: "Email:", value: "moradi.arezoo1992@gmail.com" },
  ];
  const createKeys = (componentName, specialValue) => {
    return `${specialValue} ${componentName} `;
  };
  return (
    <div className="pt-10">
      <h3 className="text-3xl font-bold border-b-2 border-secondary-100 w-fit text-secondary-300">
        About
      </h3>
      <p className="py-6">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem culpa
        esse veritatis dolore, quibusdam, assumenda inventore cumque saepe qui,
        eius sint velit exercitationem fugit numquam nulla? Soluta laudantium
        distinctio atque?
      </p>
      <div className="flex items-start">
        <img className="w-1/4 h-auto" src="/assets/profile.jpg" />
        <div className="pl-10">
          <h4 className="text-2xl font-bold text-secondary-300 ">
            Web Developer
          </h4>
          <p className="pt-5">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet,
            obcaecati. Nisi beatae accusamus, fugit minus tempore quaerat
            pariatur nam odit exercitationem eligendi praesentium rem, voluptate
            modi distinctio vero adipisci sit!
          </p>
          <div className="grid w-full grid-cols-2 grid-rows-2 gap-2 pt-5">
            {sampleData.map((key) => {
              return (
                <div className="" key={createKeys("About", key.title)}>
                  <div className="flex items-center w-full">
                    <ArrowRight className="w-2 mr-2 fill-secondary-100" />
                    <div className="flex items-center">
                      <h5 className="font-bold">{key.title}</h5>
                      <span className="px-3 text-sm">{key.value}</span>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
