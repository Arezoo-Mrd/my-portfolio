import React, { useEffect, useState } from "react";
import firebaseCloudMessaging from "./../../firebase";
import * as firebase from "firebase/app";
import "firebase/messaging";
const Connect = () => {
  const [isTokenFound, setTokenFound] = useState(false);
  useEffect(() => {
    setToken();

    // Event listener that listens for the push notification event in the background
    if ("serviceWorker" in navigator) {
      navigator.serviceWorker.addEventListener("message", (event) => {
        console.log("event for the service worker", event);
      });
    }

    // Calls the getMessage() function if the token is there
    async function setToken() {
      try {
        const token = await firebaseCloudMessaging.init();
        if (token) {
          console.log("token", token);
          getMessage();
        }
      } catch (error) {
        console.log(error);
      }
    }
  });
  return (
    <button className="absolute z-10 flex justify-center px-5 py-2 font-bold rounded-md bottom-5 hover:bg-white-500 bg-white-300 text-secondary-500">
      Connect To Me
    </button>
  );
};

export default Connect;
