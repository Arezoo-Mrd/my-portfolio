import Home from "./../../public/assets/svgs/home.svg";
import About from "./../../public/assets/svgs/about.svg";
import Resume from "./../../public/assets/svgs/resume.svg";
import Portfolio from "./../../public/assets/svgs/portfolio.svg";
import Contact from "./../../public/assets/svgs/contact.svg";

const Menu = () => {
  return (
    <>
      <ul className="w-full pt-10 ">
        <li className="flex items-center p-3 font-semibold transition-all cursor-pointer text-white-500 hover:text-white-300 group">
          <Home className="w-8 h-5 pr-3 transition-all fill-white-100 group-hover:fill-secondary-100" />
          Home
        </li>
        <li className="flex items-center p-3 font-semibold transition-all cursor-pointer text-white-500 hover:text-white-300 group">
          <About className="w-8 h-5 pr-3 transition-all fill-white-100 group-hover:fill-secondary-100" />
          About
        </li>
        <li className="flex items-center p-3 font-semibold transition-all cursor-pointer text-white-500 hover:text-white-300 group">
          <Resume className="w-8 h-5 pr-3 transition-all fill-white-100 group-hover:fill-secondary-100" />
          Resume
        </li>
        <li className="flex items-center p-3 font-semibold transition-all cursor-pointer text-white-500 hover:text-white-300 group">
          <Portfolio className="w-8 h-5 pr-3 transition-all fill-white-100 group-hover:fill-secondary-100" />
          Portfolio
        </li>
        <li className="flex items-center p-3 font-semibold transition-all cursor-pointer text-white-500 hover:text-white-300 group">
          <Contact className="w-8 h-5 pr-3 transition-all fill-white-100 group-hover:fill-secondary-100" />
          Contact
        </li>
      </ul>
    </>
  );
};

export default Menu;
