import Linkdin from "./../../public/assets/svgs/linkdin.svg";
import Instagram from "./../../public/assets/svgs/instagram.svg";
import GitLab from "./../../public/assets/svgs/gitlab.svg";
import Whatsapp from "./../../public/assets/svgs/whatsapp.svg";
import Skype from "./../../public/assets/svgs/skype.svg";

const SocialMedia = () => {
  return (
    <div className="flex items-center justify-around w-full">
      <a href="https://www.linkedin.com/in/arezoo-moradi-445953222/">
        <Linkdin className="w-6 fill-white-100" />
      </a>
      <Skype className="w-6 fill-white-100" />
      <a href="https://gitlab.com/Arezoo-Mrd">
        <GitLab className="w-6 fill-white-100" />
      </a>
      <a href="https://www.instagram.com/arezoo.moradi92/">
        <Instagram className="w-6 fill-white-100" />
      </a>
      <a href="">
        <Whatsapp className="w-6 fill-white-100" />
      </a>
    </div>
  );
};

export default SocialMedia;
