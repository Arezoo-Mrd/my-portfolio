import React from "react";
import About from "./About";
import MainPicture from "./MainPicture";
import Portfolio from "./Portfolio";
import Skill from "./Skill";

const Content = () => {
  return (
    <div className="w-[82%] bg-white-500 text-slate-900 absolute overflow-y-auto right-0 ">
      <MainPicture />
      <div className="px-5">
        <About />
        <Skill />
        <Portfolio />
      </div>
    </div>
  );
};

export default Content;
