import React from "react";

const ProfilePic = () => {
  return (
    <div className="w-[150px] h-[150px] bg-cyan-800 p-2 rounded-full">
      <img
        className="object-cover w-full h-full rounded-full"
        src="/assets/profile.jpg"
      />
    </div>
  );
};

export default ProfilePic;
