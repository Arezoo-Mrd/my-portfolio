import React from "react";
import ProfilePic from "./ProfilePic";
import SocialMedia from "./SocialMedia";

const Profile = () => {
  return (
    <div className="flex flex-col items-center w-full">
      <ProfilePic />
      <h2 className="py-2 text-2xl font-bold text-center">Arezoo Moradi</h2>
      <div className="w-3/4 pt-2">
        <SocialMedia />
      </div>
    </div>
  );
};

export default Profile;
