import React from "react";
import SkillProgress from "../Common/SkillProgress";

const Skill = () => {
  const AllSkills = [
    { id: 0, item: "HTML", percent: 90 },
    { id: 0, item: "CSS", percent: 70 },
    { id: 0, item: "Javascript", percent: 70 },
    { id: 0, item: "React", percent: 90 },
    { id: 0, item: "Redux", percent: 100 },
    { id: 0, item: "Bootstrap", percent: 90 },
    { id: 0, item: "Tailwind", percent: 60 },
    { id: 0, item: "Node.js", percent: 20 },
  ];
  return (
    <div className="pt-10">
      <h3 className="text-3xl font-bold border-b-2 border-secondary-100 w-fit text-secondary-300">
        Skills
      </h3>
      <p className="py-6">
        Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex
        aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos
        quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat
        sit in iste officiis commodi quidem hic quas.
      </p>
      <div className="grid grid-cols-2 pt-5 gap-x-3 ">
        {AllSkills.map((s) => (
          <SkillProgress key={s.id} item={s.item} percent={s.percent} />
        ))}
      </div>
    </div>
  );
};

export default Skill;
