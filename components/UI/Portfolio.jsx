import React from "react";
import Tabs from "../Common/Tabs";

const Portfolio = () => {
  return (
    <div>
      <h3 className="text-3xl font-bold border-b-2 border-secondary-100 w-fit text-secondary-300">
        Skills
      </h3>
      <p className="py-6">
        Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex
        aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos
        quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat
        sit in iste officiis commodi quidem hic quas.
      </p>
      <Tabs />
    </div>
  );
};

export default Portfolio;
