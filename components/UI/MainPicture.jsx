import React from "react";

const MainPicture = () => {
  return (
    <div
      className="relative w-full h-screen bg-fixed bg-center bg-no-repeat bg-cover main-picture "
      style={{ backgroundImage: `url('/assets/myprof.jpg')` }}
    >
      {/* <img className="object-cover w-full h-screen" src="/assets/myprof.jpg" /> */}
      <div className="absolute top-0 right-0 w-full h-full bg-black opacity-30"></div>
      <div className="absolute z-10 -translate-y-1/3 -translate-x-1/4 top-1/3 left-1/4">
        <h2 className="text-5xl font-bold text-center text-white-300 ">
          Arezoo Moradi
        </h2>
        <div className="mt-5 text-3xl font-semibold text-white-300 typing">
          I&apos;m developer
        </div>
      </div>
    </div>
  );
};

export default MainPicture;
