import Connect from "./Connect";
import Menu from "./Menu";
import Profile from "./Profile";

const Sidebar = () => {
  return (
    <div className="w-[18%]  h-screen bg-secondary-300 text-white-300 p-6 fixed left-0">
      <div className="flex flex-col justify-center">
        <Profile />
        <Menu />
        <div className="flex justify-center w-full">
          <Connect />
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
