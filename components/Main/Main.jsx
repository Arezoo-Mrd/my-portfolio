import React, { useEffect } from "react";
import PushNotificationLayout from "../PushNotificationLayout";
import Content from "../UI/Content";
import Sidebar from "../UI/Sidebar";

const Main = () => {
  useEffect(() => {
    console.log("aaa");
  }, []);

  return (
    <PushNotificationLayout>
      <div className="flex w-full">
        <Sidebar />

        {/* contents */}
        <Content />
      </div>
    </PushNotificationLayout>
  );
};

export default Main;
